package com.dataart.restcourse.api;

import com.dataart.restcourse.RestCourseApp;
import com.dataart.restcourse.model.Hotel;
import com.dataart.restcourse.model.Room;
import com.dataart.restcourse.model.enums.HotelBookingType;
import com.dataart.restcourse.model.enums.HotelExtras;
import com.dataart.restcourse.model.enums.RoomExtras;
import com.dataart.restcourse.model.enums.RoomType;
import com.dataart.restcourse.repository.HotelRepository;
import com.dataart.restcourse.repository.RoomRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = RestCourseApp.class)
@AutoConfigureMockMvc
public class BookingControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private HotelRepository hotelRepository;

    @Autowired
    private RoomRepository roomRepository;

    private static final ObjectMapper om = new ObjectMapper();

    private Hotel testStandardHotel;
    private Hotel testSanatoriumHotel;

    @Before
    public void initData() {
        Hotel hotel = new Hotel("Eliot's motel",
                "https://images.unsplash.com/photo-1562101806-b8ebd0e33011?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
                Set.of(HotelExtras.SWIMMING_POOL, HotelExtras.TENNIS_COURT),
                HotelBookingType.NORMAL);
        testStandardHotel = hotelRepository.save(hotel);
        Hotel sanatoriumHotel = new Hotel("Eliot's motel",
                "https://images.unsplash.com/photo-1562101806-b8ebd0e33011?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
                Set.of(HotelExtras.SWIMMING_POOL, HotelExtras.TENNIS_COURT),
                HotelBookingType.SANATORIUM);
        testSanatoriumHotel = hotelRepository.save(sanatoriumHotel);
    }

    @Test
    public void createBookingStandartHotelReturnsCreated() throws Exception {
        Room room = roomRepository.save(new Room(String.valueOf(1), new BigDecimal(20.00), testStandardHotel,
                Set.of(RoomExtras.TV, RoomExtras.CONDITIONER), RoomType.DOUBLE));

        LocalDate currentDate = LocalDate.now();
        BookRoomRequestTestDto requestBody = new BookRoomRequestTestDto(room.getId(),
                currentDate.plusDays(3).format(DateTimeFormatter.ISO_DATE),
                currentDate.plusDays(5).format(DateTimeFormatter.ISO_DATE));

        mockMvc.perform(post("/booking")
                .content(om.writeValueAsString(requestBody))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void createBookingInvalidDatesReturnsBadRequest() throws Exception {
        Room room = roomRepository.save(new Room(String.valueOf(2), new BigDecimal(20.00), testStandardHotel,
                Set.of(RoomExtras.TV, RoomExtras.CONDITIONER), RoomType.DOUBLE));

        LocalDate currentDate = LocalDate.now();

        // Start date is before current date
        BookRoomRequestTestDto requestBody = new BookRoomRequestTestDto(room.getId(),
                currentDate.minusDays(3).format(DateTimeFormatter.ISO_DATE),
                currentDate.plusDays(5).format(DateTimeFormatter.ISO_DATE));

        mockMvc.perform(post("/booking")
                .content(om.writeValueAsString(requestBody))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        // Finish date is before start date
        requestBody = new BookRoomRequestTestDto(room.getId(),
                currentDate.plusDays(3).format(DateTimeFormatter.ISO_DATE),
                currentDate.plusDays(1).format(DateTimeFormatter.ISO_DATE));

        mockMvc.perform(post("/booking")
                .content(om.writeValueAsString(requestBody))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void createBookingForAlreadyBookedDatedReturnsBadRequest() throws Exception {
        Room room = roomRepository.save(new Room(String.valueOf(3), new BigDecimal(20.00), testStandardHotel,
                Set.of(RoomExtras.TV, RoomExtras.CONDITIONER), RoomType.DOUBLE));

        LocalDate currentDate = LocalDate.now();

        BookRoomRequestTestDto requestBody = new BookRoomRequestTestDto(room.getId(),
                currentDate.plusDays(3).format(DateTimeFormatter.ISO_DATE),
                currentDate.plusDays(5).format(DateTimeFormatter.ISO_DATE));

        mockMvc.perform(post("/booking")
                .content(om.writeValueAsString(requestBody))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());

        // same dates
        requestBody = new BookRoomRequestTestDto(
                room.getId(),
                currentDate.plusDays(3).format(DateTimeFormatter.ISO_DATE),
                currentDate.plusDays(5).format(DateTimeFormatter.ISO_DATE));

        mockMvc.perform(post("/booking")
                .content(om.writeValueAsString(requestBody))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        // startDate can't be booked
        requestBody = new BookRoomRequestTestDto(room.getId(),
                currentDate.plusDays(5).format(DateTimeFormatter.ISO_DATE),
                currentDate.plusDays(9).format(DateTimeFormatter.ISO_DATE));

        mockMvc.perform(post("/booking")
                .content(om.writeValueAsString(requestBody))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

        // booking between dates of saved booking
        requestBody = new BookRoomRequestTestDto(room.getId(),
                currentDate.plusDays(4).format(DateTimeFormatter.ISO_DATE),
                currentDate.plusDays(4).format(DateTimeFormatter.ISO_DATE));

        mockMvc.perform(post("/booking")
                .content(om.writeValueAsString(requestBody))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void bookingSanatoriumForProperDatesReturnsCreated() throws Exception {
        Room room = roomRepository.save(new Room(String.valueOf(4), new BigDecimal(20.00), testSanatoriumHotel,
                Set.of(RoomExtras.TV, RoomExtras.CONDITIONER), RoomType.DOUBLE));
        roomRepository.save(room);
        LocalDate date = LocalDate.now();
        while (date.getDayOfWeek()!= DayOfWeek.MONDAY){
            date = date.plusDays(1);
        }

        // Start date is before current date
        BookRoomRequestTestDto requestBody = new BookRoomRequestTestDto(room.getId(),
                date.format(DateTimeFormatter.ISO_DATE),
                date.plusDays(6).format(DateTimeFormatter.ISO_DATE));

        mockMvc.perform(post("/booking")
                .content(om.writeValueAsString(requestBody))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    public void bookingSanatoriumWithInvalidDateReturnsBadRequest() throws Exception {
        Room room = roomRepository.save(new Room(String.valueOf(5), new BigDecimal(20.00), testSanatoriumHotel,
                Set.of(RoomExtras.TV, RoomExtras.CONDITIONER), RoomType.DOUBLE));
        roomRepository.save(room);
        LocalDate currentDate = LocalDate.now();

        // Start date is before current date
        BookRoomRequestTestDto requestBody = new BookRoomRequestTestDto(room.getId(),
                currentDate.plusDays(1).format(DateTimeFormatter.ISO_DATE),
                currentDate.plusDays(3).format(DateTimeFormatter.ISO_DATE));

        mockMvc.perform(post("/booking")
                .content(om.writeValueAsString(requestBody))
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }
}

class BookRoomRequestTestDto {
    private String customerName;
    private String customerPhoneNumber;
    private long roomId;

    private String startDate;
    private String finishDate;

    public BookRoomRequestTestDto() {
    }

    public BookRoomRequestTestDto(long roomId, String startDate, String finishDate) {
        this.customerName = "Ivan";
        this.customerPhoneNumber = "+3809923123";
        this.roomId = roomId;
        this.startDate = startDate;
        this.finishDate = finishDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(String finishDate) {
        this.finishDate = finishDate;
    }
}