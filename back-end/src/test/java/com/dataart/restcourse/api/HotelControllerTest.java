package com.dataart.restcourse.api;

import com.dataart.restcourse.RestCourseApp;
import com.dataart.restcourse.api.dto.GetHotelsResponseDto;
import com.dataart.restcourse.model.Hotel;
import com.dataart.restcourse.model.enums.HotelBookingType;
import com.dataart.restcourse.model.enums.HotelExtras;
import com.dataart.restcourse.repository.HotelRepository;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.test.context.junit4.SpringRunner;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.CoreMatchers.*;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = RestCourseApp.class)
@AutoConfigureMockMvc
public class HotelControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private HotelRepository hotelRepository;

    @Test
    public void getAllHotelsReturnsAllHotels() throws Exception {
        String firstHotelName = "Grand budapesht";
        Hotel hotel1 = new Hotel(firstHotelName,
                "https://images.unsplash.com/photo-1562101806-b8ebd0e33011?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
                Set.of(HotelExtras.SWIMMING_POOL, HotelExtras.TENNIS_COURT),
                HotelBookingType.NORMAL);
        String secondHotelName = "5 zirok";
        Hotel hotel2 = new Hotel(secondHotelName,
                "https://images.unsplash.com/photo-1562101806-b8ebd0e33011?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
                Set.of(HotelExtras.TENNIS_COURT, HotelExtras.WATER_PARK), HotelBookingType.NORMAL);
        String thirdHotelName = "MyHostel";
        Hotel hotel3 = new Hotel(thirdHotelName,
                "https://images.unsplash.com/photo-1562101806-b8ebd0e33011?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
                Set.of(HotelExtras.WATER_PARK), HotelBookingType.SANATORIUM);
        List<Hotel> hotels = Arrays.asList(hotel1, hotel2, hotel3);
        hotelRepository.saveAll(hotels);

        mockMvc.perform(get("/hotels/0/10"))
                .andExpect(status().isOk());
    }


    private long saveHotel(Hotel hotel) {
        return hotelRepository.save(hotel).getId();
    }
}
