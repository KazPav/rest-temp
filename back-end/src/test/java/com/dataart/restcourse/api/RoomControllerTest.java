package com.dataart.restcourse.api;

import com.dataart.restcourse.RestCourseApp;
import com.dataart.restcourse.model.Hotel;
import com.dataart.restcourse.model.Room;
import com.dataart.restcourse.model.enums.HotelBookingType;
import com.dataart.restcourse.model.enums.HotelExtras;
import com.dataart.restcourse.model.enums.RoomExtras;
import com.dataart.restcourse.model.enums.RoomType;
import com.dataart.restcourse.repository.HotelRepository;
import com.dataart.restcourse.repository.RoomRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK, classes = RestCourseApp.class)
@AutoConfigureMockMvc
public class RoomControllerTest {

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private HotelRepository hotelRepository;

    @Test
    public void getRoomsOfHotelReturnsRooms() throws Exception {
        final int NUMBER_OF_ROOMS_PER_HOTEL = 10;
        Hotel hotel = new Hotel("Eliot's motel",
                "https://images.unsplash.com/photo-1562101806-b8ebd0e33011?ixlib=rb-1.2.1&auto=format&fit=crop&w=1350&q=80",
                Set.of(HotelExtras.SWIMMING_POOL, HotelExtras.TENNIS_COURT),
                HotelBookingType.NORMAL);
        hotel = hotelRepository.save(hotel);

        List<Room> roomsOfHotel = new ArrayList<>();
        for (int i = 1; i <= NUMBER_OF_ROOMS_PER_HOTEL; i++) {
            if (i > 3 && i < 6) {
                roomsOfHotel.add(new Room(String.valueOf(i), new BigDecimal(20.00),hotel,
                        Set.of(RoomExtras.TV, RoomExtras.CONDITIONER), RoomType.DOUBLE));
            } else if (i > 6 && i < 8) {
                roomsOfHotel.add(new Room(String.valueOf(i),new BigDecimal(20.00), hotel,
                        Set.of(RoomExtras.BALCONY, RoomExtras.SEA_VIEW, RoomExtras.TV), RoomType.TWIN));
            } else {
                roomsOfHotel.add(new Room(String.valueOf(i),new BigDecimal(20.00), hotel,
                        Set.of(RoomExtras.TV, RoomExtras.TRASHCAN_VIEW, RoomExtras.CONDITIONER), RoomType.SINGLE));
            }
        }
        roomRepository.saveAll(roomsOfHotel);
        String requestUrl = "/hotel/" + hotel.getId() + "/rooms";
        mockMvc.perform(get(requestUrl))
                .andExpect(status().isOk());
    }

}
