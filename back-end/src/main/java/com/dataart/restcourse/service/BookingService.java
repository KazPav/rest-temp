package com.dataart.restcourse.service;

import com.dataart.restcourse.api.dto.BookRoomRequestDto;
import com.dataart.restcourse.api.dto.RoomBookingsDto;
import com.dataart.restcourse.model.Booking;
import com.dataart.restcourse.model.Customer;
import com.dataart.restcourse.repository.BookingRepository;
import com.dataart.restcourse.repository.CustomerRepository;
import com.dataart.restcourse.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BookingService {

    private BookingRepository bookingRepository;

    private CustomerRepository customerRepository;

    private RoomRepository roomRepository;

    public List<RoomBookingsDto> getRoomBookings(long roomId) {
        return bookingRepository.findByRoomId(roomId).stream().map(this::convertBookingToDto).collect(Collectors.toList());
    }

    public void createBooking(BookRoomRequestDto bookRoomRequestDto) {
        Booking booking = convertDtoToBooking(bookRoomRequestDto);
        bookingRepository.save(booking);
    }

    private RoomBookingsDto convertBookingToDto(Booking booking) {
        return new RoomBookingsDto(booking.getStartDate(), booking.getFinishDate());
    }

    private Booking convertDtoToBooking(BookRoomRequestDto bookRoomRequestDto) {
        return new Booking(bookRoomRequestDto.getStartDate(), bookRoomRequestDto.getFinishDate(),
                roomRepository.getOne(bookRoomRequestDto.getRoomId()),
                createOrUpdateCustomer(bookRoomRequestDto.getCustomerName(), bookRoomRequestDto.getCustomerPhoneNumber()));
    }

    private Customer createOrUpdateCustomer(String name, String phoneNumber) {
        Optional<Customer> customerOptional = customerRepository.findByPhoneNumber(phoneNumber);
        if (customerOptional.isEmpty()) {
            Customer customer = new Customer(name, phoneNumber);
            customerRepository.save(customer);
            return customer;
        } else {
            Customer customer = customerOptional.get();
            if (!customer.getName().equals(name)) {
                customer.setName(name);
                customerRepository.save(customer);
            }
            return customer;
        }
    }

    @Autowired
    public void setRoomRepository(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }

    @Autowired
    public void setCustomerRepository(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Autowired
    public void setBookingRepository(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }
}
