package com.dataart.restcourse.service;

import com.dataart.restcourse.api.dto.GetRoomsResponseDto;
import com.dataart.restcourse.model.Room;
import com.dataart.restcourse.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RoomService {

    private RoomRepository roomRepository;

    public List<GetRoomsResponseDto> getRoomsOfHotel(long hotelId) {
        return roomRepository.findByHotelId(hotelId).stream().map(this::convertRoomToRoomsDto).collect(Collectors.toList());
    }

    private GetRoomsResponseDto convertRoomToRoomsDto(Room room) {
        return new GetRoomsResponseDto(room.getId(), room.getRoomNumber(), room.getPrice(), room.getRoomExtras(), room.getRoomType());
    }

    @Autowired
    public void setRoomRepository(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }
}
