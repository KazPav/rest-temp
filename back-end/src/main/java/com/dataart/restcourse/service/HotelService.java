package com.dataart.restcourse.service;

import com.dataart.restcourse.api.dto.GetHotelsResponseDto;
import com.dataart.restcourse.model.Hotel;
import com.dataart.restcourse.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;


@Service
public class HotelService {
    private HotelRepository hotelRepository;

    public Page<GetHotelsResponseDto> getHotels(int pageNumber, int pageSize) {
        Page<Hotel> hotels = hotelRepository.findAll(PageRequest.of(pageNumber, pageSize));
        return hotels.map(this::convertHotelToHotelDto);
    }

    private GetHotelsResponseDto convertHotelToHotelDto(Hotel hotel) {
        return new GetHotelsResponseDto(hotel.getId(), hotel.getName(), hotel.getImageUrl(),
                hotel.getHotelExtras(), hotel.getHotelBookingType());
    }

    @Autowired
    public void setHotelRepository(HotelRepository hotelRepository) {
        this.hotelRepository = hotelRepository;
    }

}
