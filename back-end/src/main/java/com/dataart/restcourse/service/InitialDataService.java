package com.dataart.restcourse.service;

import com.dataart.restcourse.model.Hotel;
import com.dataart.restcourse.model.Room;
import com.dataart.restcourse.model.enums.HotelBookingType;
import com.dataart.restcourse.model.enums.HotelExtras;
import com.dataart.restcourse.model.enums.RoomExtras;
import com.dataart.restcourse.model.enums.RoomType;
import com.dataart.restcourse.repository.HotelRepository;
import com.dataart.restcourse.repository.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

@Service
public class InitialDataService {

    private HotelRepository hotelRepository;
    private RoomRepository roomRepository;
    private static final int NUMBER_OF_ROOMS_PER_HOTEL = 10;

    @PostConstruct
    private void insertInitialData() {
        if (hotelRepository.count() == 0L) {
            Hotel hotel1 = new Hotel("Grand budapesht",
                    "https://images.unsplash.com/photo-1553653924-39b70295f8da?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80",
                    Set.of(HotelExtras.SWIMMING_POOL, HotelExtras.TENNIS_COURT),
                    HotelBookingType.NORMAL);
            Hotel hotel2 = new Hotel("Polonia",
                    "https://images.unsplash.com/photo-1524514419275-fa942a023c92?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80",
                    Set.of(HotelExtras.TENNIS_COURT, HotelExtras.WATER_PARK), HotelBookingType.NORMAL);
            Hotel hotel3 = new Hotel("5 zirok",
                    "https://images.unsplash.com/photo-1560174326-bcebb82ad143?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1267&q=80",
                    Set.of(HotelExtras.WATER_PARK), HotelBookingType.SANATORIUM);
            List<Hotel> hotels = Arrays.asList(hotel1, hotel2, hotel3);
            hotelRepository.saveAll(hotels);

            List<Room> roomsOfCurrentHotel = new ArrayList<>();
            // Creating rooms for hotel1
            for (int i = 1; i <= NUMBER_OF_ROOMS_PER_HOTEL; i++) {
                if (i > 3 && i < 6) {
                    roomsOfCurrentHotel.add(new Room(String.valueOf(i), new BigDecimal(100.00), hotel1,
                            Set.of(RoomExtras.TV, RoomExtras.CONDITIONER), RoomType.DOUBLE));
                } else if (i > 6 && i < 8) {
                    roomsOfCurrentHotel.add(new Room(String.valueOf(i), new BigDecimal(150.00), hotel1,
                            Set.of(RoomExtras.BALCONY, RoomExtras.SEA_VIEW, RoomExtras.TV), RoomType.TWIN));
                } else {
                    roomsOfCurrentHotel.add(new Room(String.valueOf(i), new BigDecimal(120.00), hotel1,
                            Set.of(RoomExtras.TV, RoomExtras.TRASHCAN_VIEW, RoomExtras.CONDITIONER), RoomType.SINGLE));
                }
            }
            roomRepository.saveAll(roomsOfCurrentHotel);
            roomsOfCurrentHotel = new ArrayList<>();

            for (int i = 1; i <= NUMBER_OF_ROOMS_PER_HOTEL; i++) {
                if (i < 4) {
                    roomsOfCurrentHotel.add(new Room(String.valueOf(i),new BigDecimal(200.00), hotel2,
                            Set.of(RoomExtras.TV, RoomExtras.SWIMMING_POOL_VIEW, RoomExtras.BALCONY), RoomType.SINGLE));
                } else if (i < 7) {
                    roomsOfCurrentHotel.add(new Room(String.valueOf(i),new BigDecimal(220.00), hotel2,
                            Set.of(RoomExtras.SWIMMING_POOL_VIEW, RoomExtras.CONDITIONER, RoomExtras.TRASHCAN_VIEW), RoomType.TWIN));
                } else {
                    roomsOfCurrentHotel.add(new Room(String.valueOf(i), new BigDecimal(250.00),hotel2,
                            Set.of(RoomExtras.BALCONY, RoomExtras.TV, RoomExtras.SEA_VIEW), RoomType.DOUBLE));
                }
            }
            roomRepository.saveAll(roomsOfCurrentHotel);
            roomsOfCurrentHotel = new ArrayList<>();

            for (int i = 1; i <= NUMBER_OF_ROOMS_PER_HOTEL; i++) {
                if (i < 5) {
                    roomsOfCurrentHotel.add(new Room(i + "A", new BigDecimal(50.00), hotel3,
                            Set.of(RoomExtras.TV, RoomExtras.TRASHCAN_VIEW), RoomType.DOUBLE));
                } else {
                    roomsOfCurrentHotel.add(new Room(i + "B", new BigDecimal(60.00), hotel3,
                            Set.of(RoomExtras.BALCONY, RoomExtras.TRASHCAN_VIEW), RoomType.TWIN));
                }
            }
            roomRepository.saveAll(roomsOfCurrentHotel);
        }
    }

    @Autowired
    public void setHotelRepository(HotelRepository hotelRepository) {
        this.hotelRepository = hotelRepository;
    }

    @Autowired
    public void setRoomRepository(RoomRepository roomRepository) {
        this.roomRepository = roomRepository;
    }
}
