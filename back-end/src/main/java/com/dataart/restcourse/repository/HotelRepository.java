package com.dataart.restcourse.repository;

import com.dataart.restcourse.model.Hotel;
import com.dataart.restcourse.model.enums.HotelBookingType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface HotelRepository extends JpaRepository<Hotel, Long> {
    @Query(value ="select hotelBookingType from Hotel h where h.id = (select hotel from Room r where r.id = :roomId)")
    HotelBookingType getBookingTypeOfHotelByRoom(@Param("roomId") long roomId);

}
