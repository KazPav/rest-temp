package com.dataart.restcourse.api.dto.validation;

import com.dataart.restcourse.api.dto.BookRoomRequestDto;
import com.dataart.restcourse.model.enums.HotelBookingType;
import com.dataart.restcourse.repository.HotelRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.DayOfWeek;

@Component
public class SanatoriumDateValidator implements ConstraintValidator<SanatoriumDateCheck, BookRoomRequestDto> {

    private HotelRepository hotelRepository;

    @Override
    public void initialize(SanatoriumDateCheck constraintAnnotation) {

    }

    @Override
    public boolean isValid(BookRoomRequestDto value, ConstraintValidatorContext context) {
        if (hotelRepository.getBookingTypeOfHotelByRoom(value.getRoomId()) == HotelBookingType.NORMAL)
            return true;

        return value.getStartDate().getDayOfWeek() == DayOfWeek.MONDAY
                && value.getFinishDate().getDayOfWeek() == DayOfWeek.SUNDAY;
    }

    @Autowired
    public void setHotelRepository(HotelRepository hotelRepository) {
        this.hotelRepository = hotelRepository;
    }
}
