package com.dataart.restcourse.api.dto.validation;

import com.dataart.restcourse.api.dto.BookRoomRequestDto;
import com.dataart.restcourse.model.Booking;
import com.dataart.restcourse.repository.BookingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

@Component
public class RoomAvailabilityValidator implements ConstraintValidator<RoomAvailabilityValidation, BookRoomRequestDto> {
    private BookingRepository bookingRepository;

    @Override
    public boolean isValid(BookRoomRequestDto value, ConstraintValidatorContext context) {
        List<Booking> roomBookings = bookingRepository.findByRoomId(value.getRoomId());
        for (Booking booking : roomBookings) {
            if (value.getStartDate().isAfter(booking.getStartDate()) && value.getStartDate().isBefore(booking.getFinishDate())
                    || value.getFinishDate().isAfter(booking.getStartDate()) && value.getFinishDate().isBefore(booking.getFinishDate())
                    || value.getStartDate().isEqual(booking.getStartDate()) || value.getStartDate().isEqual(booking.getFinishDate())
                    || value.getFinishDate().isEqual(booking.getStartDate()) || value.getFinishDate().isEqual(booking.getFinishDate())
            ) {
                // TODO add isEqual for condition
                // TODO add can't be date in the past
                return false;
            }
        }
        return true;
    }

    @Autowired
    public void setBookingRepository(BookingRepository bookingRepository) {
        this.bookingRepository = bookingRepository;
    }
}
