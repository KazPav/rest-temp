package com.dataart.restcourse.api.dto;

import com.dataart.restcourse.model.enums.RoomExtras;
import com.dataart.restcourse.model.enums.RoomType;

import java.math.BigDecimal;
import java.util.Set;

public class GetRoomsResponseDto {

    private long id;

    private String roomNumber;

    private BigDecimal price;

    private Set<RoomExtras> roomExtras;

    private RoomType roomType;

    public GetRoomsResponseDto(long id, String roomNumber, BigDecimal price, Set<RoomExtras> roomExtras, RoomType roomType) {
        this.id = id;
        this.roomNumber = roomNumber;
        this.price = price;
        this.roomExtras = roomExtras;
        this.roomType = roomType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Set<RoomExtras> getRoomExtras() {
        return roomExtras;
    }

    public void setRoomExtras(Set<RoomExtras> roomExtras) {
        this.roomExtras = roomExtras;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }
}
