package com.dataart.restcourse.api.controller;

class Endpoints {
    // Get all hotels
    static final String HOTELS_URL = "/hotels/{pageNumber}/{pageSize}";

    // Get rooms of hotel
    static final String ROOMS_URL = "/hotel/{hotelId}/rooms";

    //  Book room
    static final String BOOKING_URL = "/booking";

    // Get bookings of room
    static final String BOOKINGS_URL = "/bookings/{roomId}";
}
