package com.dataart.restcourse.api.dto;

import com.dataart.restcourse.api.dto.validation.BookingDateValidation;
import com.dataart.restcourse.api.dto.validation.RoomAvailabilityValidation;
import com.dataart.restcourse.api.dto.validation.SanatoriumDateCheck;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@BookingDateValidation(message = "Invalid date")
@RoomAvailabilityValidation(message = "Room is not available at this date")
@SanatoriumDateCheck(message = "This type of hotel accepts only weekly bookings")
public class BookRoomRequestDto {

    @NotNull
    private String customerName;

    @NotNull
    private String customerPhoneNumber;

    @NotNull
    private long roomId;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @NotNull
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
    private LocalDate finishDate;

    public BookRoomRequestDto() {
    }

    public BookRoomRequestDto(String customerName, String customerPhoneNumber, long roomId, LocalDate startDate, LocalDate finishDate) {
        this.customerName = customerName;
        this.customerPhoneNumber = customerPhoneNumber;
        this.roomId = roomId;
        this.startDate = startDate;
        this.finishDate = finishDate;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerPhoneNumber() {
        return customerPhoneNumber;
    }

    public void setCustomerPhoneNumber(String customerPhoneNumber) {
        this.customerPhoneNumber = customerPhoneNumber;
    }

    public long getRoomId() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId = roomId;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(LocalDate finishDate) {
        this.finishDate = finishDate;
    }
}
