package com.dataart.restcourse.api.controller;

import com.dataart.restcourse.api.dto.GetRoomsResponseDto;
import com.dataart.restcourse.service.RoomService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class RoomController {

    private RoomService roomService;

    @ApiOperation(value = "Returns rooms of specified hotel")
    @GetMapping(Endpoints.ROOMS_URL)
    public ResponseEntity<List<GetRoomsResponseDto>> getRoomsOfHotel(@PathVariable("hotelId") Long hotelId) {
        return ResponseEntity.ok().body(roomService.getRoomsOfHotel(hotelId));
    }

    @Autowired
    public void setRoomService(RoomService roomService) {
        this.roomService = roomService;
    }
}
