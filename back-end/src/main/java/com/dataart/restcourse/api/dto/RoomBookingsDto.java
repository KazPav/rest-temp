package com.dataart.restcourse.api.dto;

import java.time.LocalDate;

public class RoomBookingsDto {

    private LocalDate startDate;

    private LocalDate finishDate;

    public RoomBookingsDto(LocalDate startDate, LocalDate finishDate) {
        this.startDate = startDate;
        this.finishDate = finishDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(LocalDate finishDate) {
        this.finishDate = finishDate;
    }
}
