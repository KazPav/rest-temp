package com.dataart.restcourse.api.dto.validation;

import com.dataart.restcourse.api.dto.BookRoomRequestDto;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.LocalDate;

@Component
public class BookingDateValidator implements ConstraintValidator<BookingDateValidation, BookRoomRequestDto> {
    @Override
    public boolean isValid(BookRoomRequestDto value, ConstraintValidatorContext context) {
        LocalDate currentDate = LocalDate.now();
        if (value.getStartDate().isBefore(currentDate) || value.getFinishDate().isBefore(currentDate)
                || value.getStartDate().isAfter(value.getFinishDate()))
            return false;
        return true;
    }
}
