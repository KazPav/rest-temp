package com.dataart.restcourse.api.controller;

import com.dataart.restcourse.api.dto.GetHotelsResponseDto;
import com.dataart.restcourse.service.HotelService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HotelController {

    private HotelService hotelService;

    @ApiOperation(value = "Returns all hotels")
    @GetMapping(Endpoints.HOTELS_URL)
    // todo change to requestParam
    public ResponseEntity<Page<GetHotelsResponseDto>> getHotels(@PathVariable("pageNumber") int pageNumber,
                                                                @PathVariable("pageSize") int pageSize) {
        return ResponseEntity.ok().body(hotelService.getHotels(pageNumber, pageSize));
    }

    @Autowired
    public void setHotelService(HotelService hotelService) {
        this.hotelService = hotelService;
    }
}
