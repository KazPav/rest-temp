package com.dataart.restcourse.api.controller;

import com.dataart.restcourse.api.dto.BookRoomRequestDto;
import com.dataart.restcourse.api.dto.RoomBookingsDto;
import com.dataart.restcourse.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class BookingController {

    private BookingService bookingService;

    @PostMapping(Endpoints.BOOKING_URL)
    public ResponseEntity<?> createBooking(@Valid @RequestBody BookRoomRequestDto bookRoomRequestDto) {
        bookingService.createBooking(bookRoomRequestDto);
        return ResponseEntity.status(201).build();
    }

    @GetMapping(Endpoints.BOOKINGS_URL)
    public ResponseEntity<List<RoomBookingsDto>> getBookingsOfRoom(@PathVariable("roomId") long roomId) {
        return ResponseEntity.ok().body(bookingService.getRoomBookings(roomId));
    }

    @Autowired
    public void setBookingService(BookingService bookingService) {
        this.bookingService = bookingService;
    }
}
