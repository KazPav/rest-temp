package com.dataart.restcourse.api.dto.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Constraint(validatedBy = BookingDateValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface BookingDateValidation {
    String message() default "{error.address}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
