package com.dataart.restcourse.api.dto;

import com.dataart.restcourse.model.enums.HotelBookingType;
import com.dataart.restcourse.model.enums.HotelExtras;

import java.util.Set;

public class GetHotelsResponseDto {
    private long id;
    private String name;
    private String imageUrl;
    private Set<HotelExtras> hotelExtras;
    private HotelBookingType hotelBookingType;

    public GetHotelsResponseDto(long id, String name, String imageUrl, Set<HotelExtras> hotelExtras, HotelBookingType hotelBookingType) {
        this.id = id;
        this.name = name;
        this.imageUrl = imageUrl;
        this.hotelExtras = hotelExtras;
        this.hotelBookingType = hotelBookingType;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Set<HotelExtras> getHotelExtras() {
        return hotelExtras;
    }

    public void setHotelExtras(Set<HotelExtras> hotelExtras) {
        this.hotelExtras = hotelExtras;
    }

    public HotelBookingType getHotelBookingType() {
        return hotelBookingType;
    }

    public void setHotelBookingType(HotelBookingType hotelBookingType) {
        this.hotelBookingType = hotelBookingType;
    }
}
