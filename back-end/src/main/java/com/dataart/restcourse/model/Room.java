package com.dataart.restcourse.model;

import com.dataart.restcourse.model.enums.RoomExtras;
import com.dataart.restcourse.model.enums.RoomType;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Set;

@Entity
@Table
public class Room {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(name = "ROOM_NUMBER")
    private String roomNumber;

    @Column
    private BigDecimal price;

    @ManyToOne
    private Hotel hotel;


    @Column(name = "EXTRAS")
    @ElementCollection(fetch =  FetchType.EAGER, targetClass = RoomExtras.class)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name="ROOM_EXTRAS", joinColumns = @JoinColumn(name="roomId"))
    private Set<RoomExtras> roomExtras;

    @Column(name = "ROOM_TYPE")
    @Enumerated(EnumType.STRING)
    private RoomType roomType;

    @OneToMany(mappedBy = "room")
    private Set<Booking> bookings;

    public Room() {
    }

    public Room(String roomNumber, BigDecimal price,  Hotel hotel, Set<RoomExtras> roomExtras, RoomType roomType) {
        this.roomNumber = roomNumber;
        this.price = price;
        this.hotel = hotel;
        this.roomExtras = roomExtras;
        this.roomType = roomType;
    }

    public Long getId() {
        return id;
    }

    public String getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(String roomNumber) {
        this.roomNumber = roomNumber;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public Set<RoomExtras> getRoomExtras() {
        return roomExtras;
    }

    public void setRoomExtras(Set<RoomExtras> roomExtras) {
        this.roomExtras = roomExtras;
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }
}
