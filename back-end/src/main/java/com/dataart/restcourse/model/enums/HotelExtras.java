package com.dataart.restcourse.model.enums;

public enum HotelExtras {
    SWIMMING_POOL, WATER_PARK, TENNIS_COURT
}
