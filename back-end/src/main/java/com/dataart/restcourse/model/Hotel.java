package com.dataart.restcourse.model;

import com.dataart.restcourse.model.enums.HotelBookingType;
import com.dataart.restcourse.model.enums.HotelExtras;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table
public class Hotel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @Column
    private String imageUrl;

    @OneToMany(mappedBy = "hotel")
    private Set<Room> rooms;

    @Column(name = "EXTRAS")
    @ElementCollection(fetch =  FetchType.EAGER, targetClass = HotelExtras.class)
    @Enumerated(EnumType.STRING)
    @CollectionTable(name="HOTEL_EXTRAS", joinColumns = @JoinColumn(name="hotelId"))
    private Set<HotelExtras> hotelExtras;

    @Column(name="HOTEL_BOOKING_STYLE")
    @Enumerated(EnumType.STRING)
    private HotelBookingType hotelBookingType;


    public Hotel() {
    }

    public Hotel(String name, String imageUrl, Set<HotelExtras> hotelExtras, HotelBookingType hotelBookingType) {
        this.name = name;
        this.imageUrl = imageUrl;
        this.hotelExtras = hotelExtras;
        this.hotelBookingType = hotelBookingType;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Set<Room> getRooms() {
        return rooms;
    }

    public void setRooms(Set<Room> rooms) {
        this.rooms = rooms;
    }

    public Set<HotelExtras> getHotelExtras() {
        return hotelExtras;
    }

    public void setHotelExtras(Set<HotelExtras> hotelExtras) {
        this.hotelExtras = hotelExtras;
    }

    public HotelBookingType getHotelBookingType() {
        return hotelBookingType;
    }

    public void setHotelBookingType(HotelBookingType hotelBookingType) {
        this.hotelBookingType = hotelBookingType;
    }
}
