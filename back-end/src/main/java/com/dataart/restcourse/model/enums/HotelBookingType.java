package com.dataart.restcourse.model.enums;

public enum HotelBookingType {
    NORMAL, SANATORIUM;
}
