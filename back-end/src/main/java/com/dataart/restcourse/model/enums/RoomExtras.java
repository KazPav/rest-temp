package com.dataart.restcourse.model.enums;

public enum RoomExtras {
    TV,  BALCONY, CONDITIONER, TRASHCAN_VIEW, SWIMMING_POOL_VIEW, SEA_VIEW
}
