package com.dataart.restcourse.model.enums;

public enum RoomType {
    SINGLE, DOUBLE, TWIN;
}
