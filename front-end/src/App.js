import React from "react";
import "./App.css";
import Hotel from "./containers/Hotels";
import AppHeader from "./components/AppHeader";

function App() {
  return (
    <div className="App">
      <AppHeader />
      <Hotel />
    </div>
  );
}

export default App;
