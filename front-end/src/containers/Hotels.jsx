import React, { Fragment } from "react";
import { connect } from "react-redux";
import { Formik } from "formik";
import {
  fetchHotels,
  fetchRoomsOfSelectedHotel,
  fetchCreateBooking,
  fetchRoomBookings
} from "../store/actionCreators/thunk/ajaxRequests";
import {
  setSelectedHotelId,
  setSelectedRoomId
} from "../store/actionCreators/actionCreators";
import { Row, Col } from "antd";
import PropTypes from "prop-types";
import { NUMBER_OF_HOTELS_PER_PAGE } from "../constants/index";
import HotelCard from "../components/HotelCard";
import BookingForm from "../components/BookingForm";
import RoomsCard from "../components/RoomsCard";
import * as Yup from "yup";
import moment from "moment";

const BookingSchema = Yup.object().shape({
  customerName: Yup.string().required(`Can't be empty`),
  customerPhoneNumber: Yup.string().required(`Can't be empty`),
  startDate: Yup.string().required(`Can't be empty`),
  finishDate: Yup.string().required(`Can't be empty`)
});

const mapStateToProps = state => ({
  hotels: state.hotels.allHotels,
  selectedHotelId: state.hotels.selectedHotelId,
  roomsOfSelectedHotel: state.hotels.roomsOfSelectedHotel,
  selectedRoomId: state.hotels.selectedRoomId,
  bookingsOfSelectedRoom: state.hotels.bookingsOfSelectedRoom
});

const mapDispatchToProps = dispatch => ({
  fetchHotels: pageNumber =>
    dispatch(fetchHotels(pageNumber, NUMBER_OF_HOTELS_PER_PAGE)),
  fetchRoomsOfSelectedHotel: hotelId =>
    dispatch(fetchRoomsOfSelectedHotel(hotelId)),
  resetSelectedHotelId: () => dispatch(setSelectedHotelId(null)),
  setSelectedRoomId: roomId => dispatch(setSelectedRoomId(roomId)),
  fetchCreateBooking: newBooking => dispatch(fetchCreateBooking(newBooking)),
  fetchRoomBookings: roomId => dispatch(fetchRoomBookings(roomId))
});

class Hotels extends React.Component {
  componentDidMount() {
    this.props.fetchHotels(0);
  }

  onHotelCardClick = hotelId => {
    if (this.props.selectedHotelId === hotelId) {
      this.props.resetSelectedHotelId();
    } else {
      this.props.fetchRoomsOfSelectedHotel(hotelId);
    }
  };

  onRoomCardClick = roomId => {
    this.props.setSelectedRoomId(roomId);
    this.props.fetchRoomBookings(roomId);
  };

  handleSubmitBooking = (values, actions) => {
    const newBooking = {
      customerName: values.customerName,
      customerPhoneNumber: values.customerPhoneNumber,
      startDate: moment(values.startDate).format("YYYY-MM-DD"),
      finishDate: moment(values.finishDate).format("YYYY-MM-DD"),
      roomId: this.props.selectedRoomId
    };
    this.props.fetchCreateBooking(newBooking);
    actions.resetForm();
  };

  calculateBookedDaysForRoom = () => {
    const enumerateDaysBetweenDate = (startDate, finishDate) => {
      const dates = [];
      dates.push(
        moment(startDate)
          .clone()
          .toDate()
      );
      const currDate = moment(startDate).startOf("day");
      const lastDate = moment(finishDate).startOf("day");

      while (currDate.add(1, "days").diff(lastDate) < 0) {
        dates.push(currDate.clone().toDate());
      }
      dates.push(
        moment(finishDate)
          .clone()
          .toDate()
      );
      return dates;
    };
    let bookedDates = [];
    this.props.bookingsOfSelectedRoom.forEach(
      item =>
        (bookedDates = bookedDates.concat(
          enumerateDaysBetweenDate(item.startDate, item.finishDate)
        ))
    );
    return bookedDates;
  };

  render() {
    return (
      <>
        <Row>
          <Col span={12} offset={6}>
            {this.props.hotels.map(hotel => (
              <Fragment key={hotel.id}>
                <HotelCard
                  selectedHotelId={this.props.selectedHotelId}
                  onHotelCardClick={() => this.onHotelCardClick(hotel.id)}
                  key={hotel.id}
                  hotel={hotel}
                />
                {hotel.id === this.props.selectedHotelId ? (
                  <>
                    <h3>Rooms of hotel {hotel.name}</h3>
                    {this.props.roomsOfSelectedHotel.map(room => (
                      <Fragment key={room.id}>
                        <RoomsCard
                          selectedRoomId={this.props.selectedRoomId}
                          room={room}
                          onRoomCardClick={() => this.onRoomCardClick(room.id)}
                        />
                        {room.id === this.props.selectedRoomId ? (
                          <Row
                            type="flex"
                            justify="space-around"
                            align="middle"
                          >
                            <Formik
                              initialValues={{
                                customerName: "",
                                customerPhoneNumber: "",
                                startDate: "",
                                finishDate: ""
                              }}
                              onSubmit={this.handleSubmitBooking}
                              validationSchema={BookingSchema}
                            >
                              {props => (
                                <BookingForm
                                  hotelBookingType={hotel.hotelBookingType}
                                  disabledDates={this.calculateBookedDaysForRoom()}
                                  setFieldValue={props.setFieldValue}
                                  values={props.values}
                                  errors={props.errors}
                                  touched={props.touched}
                                  handleBlur={props.handleBlur}
                                  handleChange={props.handleChange}
                                  handleSubmit={props.handleSubmit}
                                />
                              )}
                            </Formik>
                          </Row>
                        ) : null}
                      </Fragment>
                    ))}
                  </>
                ) : null}
              </Fragment>
            ))}
          </Col>
        </Row>
      </>
    );
  }
}

Hotels.propTypes = {
  hotels: PropTypes.arrayOf(PropTypes.object),
  bookingsOfSelectedRoom: PropTypes.arrayOf(PropTypes.object)
};

Hotels.defaultProps = {
  hotels: [],
  bookingsOfSelectedRoom: []
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Hotels);
