import * as types from "../actionTypes/actionTypes";

const initialState = {};
const hotels = (state = initialState, action) => {
  switch (action.type) {
    case types.SET_HOTELS:
      return {
        ...state,
        allHotels: action.hotels
      };
    case types.SET_HOTELS_PAGINATION_DATA:
      return {
        ...state,
        hotelTotalPages: action.totalPages,
        hotelCurrentPage: action.currentPage
      };
    case types.SET_SELECTED_HOTEL_ID:
      return {
        ...state,
        selectedHotelId: action.selectedHotelId
      };
    case types.SET_ROOMS_OF_SELECTED_HOTEL:
      return {
        ...state,
        roomsOfSelectedHotel: action.roomsOfSelectedHotel
      };
    case types.SET_SELECTED_ROOM_ID:
      return {
        ...state,
        selectedRoomId: action.selectedRoomId
      };
    case types.SET_BOOKINGS_OF_SELECTED_ROOM:
      return {
        ...state,
        bookingsOfSelectedRoom: action.bookingsOfSelectedRoom
      };
    default:
      return state;
  }
};

export default hotels;
