import * as types from '../actionTypes/actionTypes';

export const setHotels = hotels => ({
    type: types.SET_HOTELS,
    hotels,
});

export const setHotelsPaginationData = (totalPages, currentPage) => ({
    type: types.SET_HOTELS_PAGINATION_DATA,
    totalPages,
    currentPage,
})

export const setSelectedHotelId = selectedHotelId => ({
    type: types.SET_SELECTED_HOTEL_ID,
    selectedHotelId,
});

export const setRoomsOfSelectedHotel = roomsOfSelectedHotel => ({
    type: types.SET_ROOMS_OF_SELECTED_HOTEL,
    roomsOfSelectedHotel,
});

export const setSelectedRoomId = selectedRoomId => ({
    type: types.SET_SELECTED_ROOM_ID,
    selectedRoomId,
});

export const setBookingsOfSelectedRoom = bookingsOfSelectedRoom => ({
    type: types.SET_BOOKINGS_OF_SELECTED_ROOM,
    bookingsOfSelectedRoom,
});