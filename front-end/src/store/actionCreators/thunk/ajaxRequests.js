import axios from "axios";
import * as urls from "../../../constants/urls";
import * as actions from "../actionCreators";
import { openNotification } from "../../../constants/index";

export const fetchHotels = (pageNum, pageSize) => dispatch =>
  axios
    .get(urls.HOTELS_URL(pageNum, pageSize))
    .then(response => {
      if (response.status === 200) {
        dispatch(actions.setHotels(response.data.content));
        dispatch(
          actions.setHotelsPaginationData(
            response.data.totalPages,
            response.data.number + 1
          )
        );
      } else {
        openNotification(
          "Error",
          "Error fetching hotels. Check your connection"
        );
      }
    })
    .catch(e => {
      openNotification("Error", "Error fetching hotels. Check your connection");
      console.error(e);
    });

export const fetchRoomsOfSelectedHotel = hotelId => dispatch =>
  axios
    .get(urls.ROOMS_URL(hotelId))
    .then(response => {
      if (response.status === 200) {
        dispatch(actions.setRoomsOfSelectedHotel(response.data));
        dispatch(actions.setSelectedHotelId(hotelId));
      } else {
        console.error("Error fetching rooms");
      }
    })
    .catch(() => {
      console.error("Error fetching rooms");
    });

export const fetchRoomBookings = roomId => dispatch =>
  axios
    .get(urls.ROOM_BOOKINGS_URL(roomId))
    .then(response => {
      if (response.status === 200) {
        dispatch(actions.setBookingsOfSelectedRoom(response.data));
        dispatch(actions.setSelectedRoomId(roomId));
      } else {
        console.error("Error fetching bookings");
      }
    })
    .catch(() => {
      console.error("Error fetching bookings");
    });

export const fetchCreateBooking = booking => dispatch =>
  axios
    .post(urls.BOOKING_URL, booking)
    .then(response => {
      if (response.status === 201) {
        openNotification("Success", "Room booked");
      } else {
        openNotification("Error", "Error booking room");
      }
    })
    .catch(e => {
      console.log(e);
    });
