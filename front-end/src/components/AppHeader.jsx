import React from "react";
import { Layout } from "antd";

const { Header } = Layout;

const AppHeader = props => (
  <Header className="app-header">Hootel booking</Header>
);

export default AppHeader;
