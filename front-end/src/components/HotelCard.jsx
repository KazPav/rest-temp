import React from "react";
import { Row, Col } from "antd";
import { FaSwimmer, FaSwimmingPool, FaTableTennis } from "react-icons/fa/";

const HotelCard = props => {
  const hotelExtras = [];
  if (props.hotel.hotelExtras.includes("SWIMMING_POOL")) {
    hotelExtras.push(<FaSwimmer key="swimming_pool" title="Swimming pool" />);
  }
  if (props.hotel.hotelExtras.includes("WATER_PARK")) {
    hotelExtras.push(<FaSwimmingPool key="water_park" title="Water park" />);
  }
  if (props.hotel.hotelExtras.includes("TENNIS_COURT")) {
    hotelExtras.push(<FaTableTennis key="tennis_court" title="Tennis court" />);
  }
  return (
    <div
      className={
        props.selectedHotelId === props.hotel.id
          ? "hotel-card hotel-card-selected"
          : "hotel-card"
      }
      onClick={props.onHotelCardClick}
    >
      <Row>
        <Col span={10}>
          <img
            alt="hotelimage"
            height="100"
            width="100"
            src={props.hotel.imageUrl}
          />
        </Col>
        <Col span={14}>
          <Row>
            <p>{props.hotel.name}</p>
          </Row>
          <Row>{hotelExtras}</Row>
        </Col>
      </Row>
    </div>
  );
};

export default HotelCard;
