import React from "react";
import { Row, Col } from "antd";
import {
  FaTv,
  FaStore,
  FaSnowflake,
  FaTrash,
  FaWater,
  FaSwimmingPool
} from "react-icons/fa/";

const RoomsCard = props => {
  let roomType =
    props.room.roomType.charAt(0) + props.room.roomType.slice(1).toLowerCase();
  const roomExtras = [];
  if (props.room.roomExtras.includes("TV")) {
    roomExtras.push(<FaTv key="tv" title="TV" />);
  }
  if (props.room.roomExtras.includes("BALCONY")) {
    roomExtras.push(<FaStore key="balcony" title="Balcony" />);
  }
  if (props.room.roomExtras.includes("CONDITIONER")) {
    roomExtras.push(<FaSnowflake key="conditioner" title="Conditioner" />);
  }
  if (props.room.roomExtras.includes("TRASHCAN_VIEW")) {
    roomExtras.push(<FaTrash key="trashcan_view" title="Trashcan View" />);
  }
  if (props.room.roomExtras.includes("SEA_VIEW")) {
    roomExtras.push(<FaWater key="sea_view" title="Sea view" />);
  }
  if (props.room.roomExtras.includes("SWIMMING_POOL_VIEW")) {
    roomExtras.push(
      <FaSwimmingPool key="swimming_pool_view" title="Swimming pool view" />
    );
  }
  return (
    <div
      className={
        props.selectedRoomId === props.room.id
          ? "rooms-card rooms-card-selected"
          : "rooms-card"
      }
      onClick={props.onRoomCardClick}
    >
      <Row>
        <Col span={12}>
          <b>Room number:</b> {props.room.roomNumber}
        </Col>
        <Col span={12}>
          <b>Room type:</b> {roomType}
        </Col>
      </Row>
      <Row className="room-card-extras">
        <Col span={12}>
          <b>Room extras: {roomExtras}</b>
        </Col>
        <Col span={12}>
          <b>Price:</b> {props.room.price} $
        </Col>
      </Row>
    </div>
  );
};

export default RoomsCard;
