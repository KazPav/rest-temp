import React from "react";
import { Form, Input, Button, Row } from "antd";
import moment from "moment";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

const FilterMonday = date => {
  const day = date.getDay();
  return day === 1;
};

const FilterSunday = date => {
  const day = date.getDay();
  return day === 0;
};

const BookingForm = props => (
  <>
    {props.hotelBookingType === "SANATORIUM" ? (
      <Row>
        <h6>This hotel has sanatorium booking type </h6>
      </Row>
    ) : null}
    <Row>
      <Form onSubmit={props.handleSubmit}>
        <Form.Item label="Your name">
          <span className="error">
            {props.errors.customerName &&
              props.touched.customerName &&
              props.errors.customerName}
          </span>
          <Input
            onBlur={props.handleBlur}
            onChange={props.handleChange}
            placeholder="Name"
            name="customerName"
            value={props.values.customerName}
          />
        </Form.Item>
        <Form.Item label="Your phone number">
          <span className="error">
            {props.errors.customerPhoneNumber &&
              props.touched.customerPhoneNumber &&
              props.errors.customerPhoneNumber}
          </span>
          <Input
            onBlur={props.handleBlur}
            onChange={props.handleChange}
            placeholder="Phone number"
            name="customerPhoneNumber"
            value={props.values.customerPhoneNumber}
          />
        </Form.Item>
        <Row>
          <span className="error">
            {(props.errors.startDate &&
              props.touched.startDate &&
              props.errors.startDate) ||
              (props.errors.finishDate &&
                props.touched.finishDate &&
                props.errors.finishDate)}
          </span>
        </Row>
        <DatePicker
          dateFormat="yyyy-MM-dd"
          value={props.values.startDate ? moment(props.values.startDate) : null}
          placeholderText="Start date"
          filterDate={
            props.hotelBookingType === "SANATORIUM" ? FilterMonday : null
          }
          name="startDate"
          onChange={props.handleChange}
          onBlur={props.handleBlur}
          selected={props.values.startDate}
          excludeDates={props.disabledDates}
          onSelect={value => {
            props.setFieldValue("startDate", value);
          }}
          minDate={moment().toDate()}
          maxDate={props.values.finishDate ? props.values.finishDate : null}
        />

        <DatePicker
          dateFormat="yyyy-MM-dd"
          value={
            props.values.finishDate ? moment(props.values.finishDate) : null
          }
          placeholderText="Finish date"
          name="finishDate"
          onChange={props.handleChange}
          filterDate={
            props.hotelBookingType === "SANATORIUM" ? FilterSunday : null
          }
          excludeDates={props.disabledDates}
          onBlur={props.handleBlur}
          selected={props.values.finishDate}
          onSelect={value => {
            props.setFieldValue("finishDate", value);
          }}
          minDate={
            props.values.startDate ? props.values.startDate : moment().toDate()
          }
        />
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Submit booking
          </Button>
        </Form.Item>
      </Form>
    </Row>
  </>
);

export default BookingForm;
