import { notification } from "antd";

export const NUMBER_OF_HOTELS_PER_PAGE = 5;

export const openNotification = (message, description) => {
  notification.open({
    message,
    description
  });
};
