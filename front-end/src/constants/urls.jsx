// const rootUrl = 'http://localhost:8080';

export const BOOKING_URL = `/booking`;
export const ROOM_BOOKINGS_URL = roomId => `/bookings/${roomId}`;
export const HOTELS_URL = (pageNumber, pageSize) => `/hotels/${pageNumber}/${pageSize}`;
export const ROOMS_URL = hotelId => `/hotel/${hotelId}/rooms`;
